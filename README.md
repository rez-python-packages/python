# Python

## Package Info


### CentOS 7 yum requirements

```bash
  # Compilers and related tools:
  yum groupinstall -y "development tools"
  # Libraries needed during compilation to enable all features of Python:
  yum install -y \
    zlib-devel \
    bzip2-devel \
    openssl-devel \
    ncurses-devel \
    sqlite-devel \
    readline-devel \
    tk-devel \
    gdbm-devel \
    db4-devel \
    libpcap-devel \
    xz-devel \
    expat-devel
```
