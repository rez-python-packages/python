# -*- coding: utf-8 -*-

name = 'python'

version = '2.7.14'

variants = [
    ['platform-linux', 'arch-x86_64'],
]


def commands():
    appendenv('PATH', '{root}/bin')
    if building:
        setenv('PYTHON_EXECUTABLE', '{root}/bin/python2.7')
        setenv('PYTHON_LIBRARIES', '{root}/lib/libpython2.7.so')
        setenv('PYTHON_INCLUDE_DIR', '{root}/include')
